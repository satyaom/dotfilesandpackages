#fzf hidden file
export FZF_DEFAULT_COMMAND='find .'

#NNN
export NNN_PLUG='i:imgview;p:preview-tui;d:dragdrop;o:fzopen;r:pdfview;j:autojump;f:fzcd'
export NNN_FCOLORS="c1e2272e006033f7c6d6abc4"
export NNN_FIFO="/tmp/nnn.fifo"
#pfetch brightness for info
export PF_COL2=8

#import flutter
export PATH="$PATH:$HOME/flutter/bin"

#Default programs
export CODEEDITOR="vscodium"
export COLORTERM="truecolor"
export PAGER="bat"
export TERMINAL="alacritty"
export WM="openbox"
export EDITOR="nvim"
export BROWSER="firefox"
export READER="wpspdf"
export FILE="nnn"
export OPENER="xdg-open"
export VIDEO="dragon"
export IMAGE="gpicview"
export VISUAL="nvim"

#Clean stuff
export LESSHISTFILE="-"
export ZDOTDIR="/$HOME/.config/zsh"

# Start blinking
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2) # green
# Start bold
export LESS_TERMCAP_md=$(tput bold; tput setaf 2) # green
# Start stand out
export LESS_TERMCAP_so=$(tput bold; tput setaf 3) # yellow
# End standout
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
# Start underline
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 1) # red
# End Underline
export LESS_TERMCAP_ue=$(tput sgr0)
# End bold, blinking, standout, underline
export LESS_TERMCAP_me=$(tput sgr0)
